<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>
<body>


	<ul class="nav justify-content-end">
		<li class="nav-item">${userInfo.name}さん　<a class="nav-link active"
			href="LogoutServlet">ログアウト</a></li>
	</ul>


	<h1>ユーザー一覧</h1>



	<form method="post" action="UserListServlet">

		<table border="0" align="center" cellpadding="15">
			<tr>

				<th></th>

				<td align="right">

					<p>
						<a href="NewRegisterServlet">新規登録</a>
					</p>
				</td>
			</tr>



			<tr>

				<th>ログインID</th>

				<td><input type="text" name="loginid" size="30"></td>
			</tr>

			<tr>

				<th>ユーザー名</th>

				<td><input type="text" name="username" size="30"></td>
			</tr>


			<tr>
				<th>生年月日</th>

				<td><input type="date" name="calendarmin" max="2019-12-31" min="1990-01-31">～
					<input type="date" name="calendarmax" max="2030-12-31" min="1990-01-31"></td>

			</tr>

		</table>
		<div style="text-align: center">
			<button class="btn btn-primary" type="submit">検索</button>
		</div>

	</form>


	<br>
	<br>


	<table border="1" align="center">
		<tr bgcolor="#c0c0c0">
			<th>ログインID</th>
			<th>ユーザ名</th>
			<th>生年月日</th>
			<th></th>
		</tr>

		<c:forEach var="user" items="${userlist}">
			<tr>
				<td align="left">${user.loginId}</td>
				<td align="left">${user.name}</td>
				<td align="left">${user.birthDate}</td>
				<td align="left"><a class="btn btn-primary"
					href="UserDetailServlet?id=${user.id}">詳細</a>
					<c:if	test="${sessionScope.userInfo.loginId== user.loginId || sessionScope.userInfo.loginId=='admin'}">
						<a class="btn btn-primary" href="UserUpdateServlet?id=${user.id}">更新</a>


					</c:if> <c:if test="${sessionScope.userInfo.loginId =='admin'}">
						<a class="btn btn-primary" href="UserDeleteServlet?id=${user.id}">削除</a>
					</c:if></td>
			</tr>
		</c:forEach>

	</table>
	</body>
</html>