<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">

</head>
<body>
<ul class="nav justify-content-end">
		<li class="nav-item">${userInfo.name}さん　<a class="nav-link active"
			href="LogoutServlet">ログアウト</a></li>
	</ul>

	<form class="form-register" action="NewRegisterServlet" method="post">
		<h1>ユーザ新規登録</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<c:if test="${passErr != null}">
			<div class="alert alert-danger" role="alert">${passErr}</div>
		</c:if>

		<table border="0" align="center" cellpadding="15">
			<tr>

				<th>ログインID</th>

				<td><input type="text" name="loginId" size="30"
					placeholder="ログインID" value="${errLoginId}"></td>
			</tr>

			<tr>
				<th>パスワード</th>

				<td><input type="password" name="pass" size="30"
					placeholder="password"></td>

			</tr>

			<tr>
				<th>パスワード(確認)</th>

				<td><input type="password" name="passConfirmation" size="30"
					placeholder="password"></td>

			</tr>


			<tr>
				<th>ユーザー名</th>

				<td><input type="text" name="username" size="30"
					placeholder="username" value="${errUserName}"></td>

			</tr>

			<tr>
				<th>生年月日</th>

				<td><input type="date" name="birthdate" value="${errBirthdate}"></td>

			</tr>

			<tr>
				<th></th>

				<td>

					<button class="btn" type="submit">登録</button>
				</td>

			</tr>

			<tr>
				<th><a href="UserListServlet">戻る</a></th>

				<td></td>

			</tr>


		</table>
	</form>


</body>
</html>