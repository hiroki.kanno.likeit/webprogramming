<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>
<body>
	<ul class="nav justify-content-end">
		<li class="nav-item">${userInfo.name}さん　<a class="nav-link active" href="LogoutServlet">ログアウト</a>
		</li>
	</ul>

	<h1>ユーザ情報詳細参照</h1>

	<div align="center">
		<table border="0" cellpadding="15">
			<tr>

				<th>ログインID</th>

				<td>
					${UserDetail.loginId }
				</td>
			</tr>

			<tr>
				<th>ユーザ名</th>

				<td>
					${UserDetail.name }
				</td>

			</tr>

			<tr>
				<th>生年月日</th>

				<td>
					${UserDetail.birthDate }
					</td>

			</tr>


			<tr>
				<th>登録日時</th>

				<td>
					${UserDetail.createDate }
				</td>

			</tr>

			<tr>
				<th>更新日時</th>

				<td>
					${UserDetail.updateDate }
				</td>

			</tr>

			<tr>

				<th><a href="UserListServlet">戻る</a></th>

			</tr>



		</table>
		</div>
</body>
</html>