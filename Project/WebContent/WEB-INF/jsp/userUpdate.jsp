<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">

</head>
<body>

	<ul class="nav justify-content-end">
		<li class="nav-item">${userInfo.name}さん　<a class="nav-link active" href="LogoutServlet">ログアウト</a>
		</li>
	</ul>


	<form method="post" action="UserUpdateServlet">
		<h1>ユーザ情報更新</h1>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<c:if test="${passErr != null}">
			<div class="alert alert-danger" role="alert">${passErr}</div>
		</c:if>

		<table border="0" align="center" cellpadding="15">
			<tr>


		<table border="0" align="center" cellpadding="15">
			<tr>

				<th>ログインID</th>

				<td>${UserUpdate.loginId}</td>
			</tr>

			<tr>
				<th>パスワード</th>

				<td><input type="password" name="password" size="30"
					placeholder="password"></td>

			</tr>

			<tr>
				<th>パスワード(確認)</th>

				<td><input type="password" name="passwordConfirmation" size="30"
					placeholder="password"></td>

			</tr>


			<tr>
				<th>ユーザー名</th>

				<td><input type="text" name="username" size="30" value="${UserUpdate.name}">
				</td>

			</tr>

			<tr>
				<th>生年月日</th>

				<td><input type="date" name="birthdate" max="2050-12-31"
					value="${UserUpdate.birthDate}"></td>
				</tr>

			<tr>
				<th></th>

				<td>
					<button class="btn" type="submit"value="${UserUpdate.id}" name="id">更新</button>
				</td>

			</tr>

			<tr>
				<th><a href="UserListServlet">戻る</a></th>

				<td></td>

			</tr>


		</table>

	</form>


</body>
</html>