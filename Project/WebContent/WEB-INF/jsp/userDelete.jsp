<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">

</head>
<body>

	<ul class="nav justify-content-end">
		<li class="nav-item">${userInfo.name}さん　<a class="nav-link active" href="LogoutServlet">ログアウト</a>
		</li>
	</ul>

	<h1>ユーザ削除確認</h1>
	<div class="container text-center">

		<p>ユーザID:${UserDelete.id}を消去しますか？</p>

		<form class="form-delete" action="UserDeleteServlet" method="post">
			<div class=delete>
				<a href="UserListServlet" class="btn btn-light">いいえ</a>
				<button class="btn" type="submit" value="${UserDelete.id}"
					name="deleteform">はい</button>

			</div>
		</form>
	</div>

</body>
</html>