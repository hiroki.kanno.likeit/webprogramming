<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link href="css/login.css" rel="stylesheet">
</head>
<body>

	<h1>ログイン画面</h1>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<form class="form-signin" action="LoginServlet" method="post">
		<div align="center">
			<table border="0" cellpadding="15">
				<tr>

					<th>ログインID</th>

					<td><input type="text" name="loginId" size="30"
						placeholder="ログインID"></td>
				</tr>

				<tr>
					<th>パスワード</th>

					<td><input type="password" name="password" size="30"
						placeholder="password"></td>

				</tr>

			</table>
			<button class="btn btn-primary" type="submit">ログイン</button>
	</form>

	</div>
</body>
</html>