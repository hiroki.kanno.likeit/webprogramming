package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int id;
	private String login_id;
	private String name;
	private Date birth_date;
	private String password;
	private String create_date;
	private String update_date;

	// ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String name) {
		this.login_id = loginId;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String login_id, String name, Date birth_date, String password, String create_date,
			String update_date) {
		this.id = id;
		this.login_id = login_id;
		this.name = name;
		this.birth_date = birth_date;
		this.password = password;
		this.create_date = create_date;
		this.update_date = update_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birth_date;
	}
	public void setBirthDate(Date birth_date) {
		this.birth_date = birth_date;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return create_date;
	}
	public void setCreateDate(String create_date) {
		this.create_date = create_date;
	}
	public String getUpdateDate() {
		return update_date;
	}
	public void setUpdateDate(String update_date) {
		this.update_date = update_date;
	}

	//ユーザ詳細用
	public User(int id,String login_id, String name, Date birth_date,String create_date,
			String update_date) {
		this.id=id;
		this.login_id = login_id;
		this.name = name;
		this.birth_date = birth_date;
		this.create_date = create_date;
		this.update_date = update_date;
	}

	//パスワード暗号化用のbeans
	

}
