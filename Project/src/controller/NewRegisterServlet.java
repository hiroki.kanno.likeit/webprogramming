package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class NewRegisterServlet
 */
@WebServlet("/NewRegisterServlet")
public class NewRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインセッションがない場合ログイン画面へ
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {

			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newRegister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("pass");
		String passwordConfirmation = request.getParameter("passConfirmation");
		String username = request.getParameter("username");
		String birthdate = request.getParameter("birthdate");

		/** 空欄があった場合 **/

		if (loginId.isEmpty() || password.isEmpty() || username.isEmpty() || birthdate.isEmpty()) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "空欄があったため登録に失敗しました。");
			request.setAttribute("errLoginId", loginId);
			request.setAttribute("errUserName", username);
			request.setAttribute("errBirthdate", birthdate);

			// 新規登録にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newRegister.jsp");
			dispatcher.forward(request, response);
			return;
		} else

		/** パスワードが一致しなかった場合 **/

		if (!(password.equals(passwordConfirmation))) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("passErr", "パスワードが一致しないため登録に失敗しました。");
			request.setAttribute("errLoginId", loginId);
			request.setAttribute("errUserName", username);
			request.setAttribute("errBirthdate", birthdate);

			// 新規登録にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newRegister.jsp");
			dispatcher.forward(request, response);
			return;
		} else {

			/*
			 * UserDao secretuserDao = new UserDao(); String secretpassword =
			 * secretuserDao.SecretPass(password);
			 */

			UserDao userDao = new UserDao();
			userDao.NewUserRegister(loginId, password, username, birthdate);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}

	}
}