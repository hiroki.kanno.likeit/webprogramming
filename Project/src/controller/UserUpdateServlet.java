package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログインセッションがない場合ログイン画面へ
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {

			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// URLからGETパラメータとしてIDを受け取る

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.UserDetail(id);

		request.setAttribute("UserUpdate", user);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String ID = request.getParameter("id");
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");
		String username = request.getParameter("username");
		String birthdate = request.getParameter("birthdate");

		// ユーザーネームまたは誕生日に空欄があった場合
		if (username.isEmpty() || birthdate.isEmpty()) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ユーザー名または生年月日を入力して下さい");
			request.setAttribute("errUserName", username);
			request.setAttribute("errBirthdate", birthdate);

			// ユーザ更新にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// パスワードが一致しなかった場合

		else if (!(password.equals(passwordConfirmation))) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("passErr", "パスワードが一致しないため更新に失敗しました。");
			request.setAttribute("errUserName", username);
			request.setAttribute("errBirthdate", birthdate);

			// ユーザ更新にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// パスワードが入力されて合致している場合、パスワードの更新とユーザ名誕生日を更新
		else if ((password.equals(passwordConfirmation)) & !(password.isEmpty()) || !(passwordConfirmation.isEmpty())) {

			// 更新のdaoを実行
			UserDao userDao = new UserDao();
			userDao.UserUpdate(ID, username, birthdate, password);

			// ユーザ一覧にフォワード
			response.sendRedirect("UserListServlet");
		} else
		// パスワードが空でユーザネームと誕生日が入力されている場合
		{
			UserDao userDao = new UserDao();
			userDao.UserUpdatePasswordNull(ID, username, birthdate);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");
		}

	}
}
